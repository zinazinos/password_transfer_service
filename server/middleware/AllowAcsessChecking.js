const {Authentication}=require('../models/model')
const ApiError=require('../error/apiError')
module.exports=async function(req,res,next){ //проверка всех введенных данных пользователем, который получает пароль: корректность ссылки, дату истекания, количество открытий
    const link1 ='http://localhost:5000/api/decrypt/'+String(req.params.link1)
    const URLs = await Authentication.findOne({
        where: {link:`${link1}`}
    })
    if(!URLs){
        return next(ApiError.badRequest('Неправильная или нерабочая ссылка!'))
    }

    let expiration_time = URLs.expiration_time
    expiration_time=expiration_time.toISOString()
    let datenow = new Date()
    datenow = datenow.toISOString()
    if(expiration_time<datenow){
        await Authentication.destroy({
            where: {link:`${link1}`}
        })
        return next(ApiError.badRequest('Истек срок использования ссылки и ключа'))
    }

    let openings_left1 = URLs.openings_left
    if(openings_left1==1){
        await Authentication.destroy({
            where: {link:`${link1}`}
        })
    }
    openings_left1=openings_left1-1
    await Authentication.update(
        {
            openings_left: openings_left1
        },
        {
        where: {link:`${link1}`}
        })
    next()
}
