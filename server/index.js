//главный файл - сервер
require('dotenv').config()              //подключение переменного окружения
const express = require('express')      //фреймворк веб-приложений
const sequelize = require('./db')       //пакет для подключения бд
const models = require('./models/model')//модели бд
const cors = require('cors')            //cors
const router=require('./routes/index')  //маршруты на сервере
const errorHandler = require('./middleware/errorHandlingMiddleware')//обработчик ошибок 1
const metricsController = require('./controllers/metrics_controller')//метрики prometheus
const routes_err = require('./error/routes_error')//обработчик ошибок 2


const PORt = process.env.PORT||5000

const app = express()
app.use(cors())
app.use(express.json())
app.use('/api', router)

app.get('/metrics', metricsController.setmetrics)
//Обработки ошибок- последние обработки Middleware-ами
app.use('/', routes_err)
app.use(errorHandler)

const start = async()=>{
    try{
        await sequelize.authenticate()
        await sequelize.sync()
        app.listen(PORt, ()=>{console.log(`server started on port ${PORt}`)})
    }
    catch(e){
        console.log(e)
    }
}  
start()
