let rsa = require('node-rsa')
let fs = require('fs')

function Generate_key_pair(){ //метод создания секретных ключей для rsa шифрования пароля и ключа при отправке в БД.
    let key = new rsa().generateKeyPair()       //этот файл запускается отдельно 1 раз. (нужно перейти в эту директорию и запустить команду node creating_secret_key.js)

    let publicKey = key.exportKey("public")
    let privateKey = key.exportKey("private")

    fs.openSync('./secret_key.pem', 'w')
    fs.writeFileSync('./secret_key.pem', privateKey, 'utf-8')

    fs.openSync('./public_key.pem', 'w')
    fs.writeFileSync('./public_key.pem', publicKey, 'utf-8')
}
Generate_key_pair()