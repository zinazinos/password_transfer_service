const rsa = require('node-rsa')
const fs = require('fs')
const path = require('path')

function cipher(text){      //шифрование публичным ключом для последующей отправки на сервер
    let publicKey = new rsa()
    var public=fs.readFileSync(path.resolve(__dirname,'public_key.pem'), 'utf-8')
    publicKey.importKey(public)
    
    const encrypted = String(publicKey.encrypt(text, 'base64'))
    return encrypted
}
function decipher(encrypted){       //расшифровка секретным ключом того, что пришло с сервера
    let privatecKey = new rsa()
    var private=fs.readFileSync(path.resolve(__dirname,'secret_key.pem'), 'utf-8')
    privatecKey.importKey(private)
    
    const decrypted =  privatecKey.decrypt(encrypted, 'utf8')
    return decrypted
}
//console.log(typeof(cipher('test1йцувйцуайцуайцуайцу12у1у412у12у12у1')))    
module.exports = {cipher,decipher}
