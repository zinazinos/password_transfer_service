const express = require('express')
const router = express.Router()

router.use(async (req, res) => {        //крайний middleware, обрабатывающий ошибки (через этот ничего не пройдет)
    try {
        const route = require(`.${req.path}`)[req.method]       
    } catch (err) {        
        res.status(404).send({
            error: 'NOT_FOUND',
            description: 'Неверная ссылка.',
        })
    }
})

module.exports = router