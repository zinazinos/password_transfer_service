const Router = require('express')    // роутер для encrypt с возможностью расширения
const router = new Router()
const authController = require('../controllers/auth_controller')


router.post('/', authController.sendPasswordOnServer)

module.exports = router


