const Router = require('express')    // роутер для decrypt с возможностью расширения
const router = new Router()
const deauthController = require('../controllers/deauth_controller')
const AllowAcsessChecking = require('../middleware/AllowAcsessChecking')

router.get('/:link1', AllowAcsessChecking, deauthController.openingPostRequest)
router.post('/:link1', deauthController.getPasswordFromServer)
module.exports = router
//outputAllURL

