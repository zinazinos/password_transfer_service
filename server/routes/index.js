const Router = require('express')  // общий роутер для api с возможностью расширения
const router = new Router()

const auth_router = require('./auth_router')
const deauth_router=require('./deauth_router')
//const metrics_router=require('./metrics_router')  //метрики были пересены из /api/metrics в /metrics


router.use('/encrypt', auth_router)
router.use('/decrypt', deauth_router)
//router.use('/metrics', metrics_router)

module.exports = router