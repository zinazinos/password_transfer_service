const sequelize = require('../db')
const {DataTypes}=require('sequelize')


const Authentication = sequelize.define('authentification',{    //модель sequelize для настройки БД
    id:{type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    key:{type: DataTypes.STRING},
    link:{type: DataTypes.STRING},
    password:{type: DataTypes.STRING},
    expiration_time:{type: DataTypes.DATE},
    openings_left:{type:DataTypes.INTEGER}
})


module.exports = {
    Authentication
}