const {Authentication}=require('../models/model')
const ApiError=require('../error/apiError')
const {keys_generator} = require('../generator/keys_generator')
const {cipher} = require('../generator/hashing')
const Metrics = require('../controllers/metrics_controller')

class AuthController{
    
    async sendPasswordOnServer(req,res,next){ //отправка в бд шифрованного пароля, ключа, ссылки и др. необходимых данных из запроса
        try{
            const start = new Date().getTime();
            let {password1, expiration_time, openings_left} = req.body
            let key = String(keys_generator(15))
            openings_left=Number(openings_left)
            let password = cipher(password1)
            let link = 'http://localhost:5000/api/decrypt/'+ keys_generator(12)
            const auth = await Authentication.create({key,link,password,expiration_time,openings_left})

            let handler = "/api/encrypt"; let code = "200"; let method = "POST";let time = 2.2  //метрики
            Metrics.setGauge(handler, code, method, time)

            res.json(auth)
        }
        catch(e){
            next(ApiError.badRequest(e.message))
        }
    }
}
module.exports = new AuthController()