const {Authentication}=require('../models/model')
const ApiError=require('../error/apiError')
const {decipher} = require('../generator/hashing')
const Metrics = require('../controllers/metrics_controller')

let canGetPas = false
class AuthController{
    openingPostRequest(req, res, next){     //сначала пользователь отправляет GET (заходит на страницу)
        canGetPas=true
        let handler = `/api/decrypt/${req.params.link1}`; let code = "200"; let method = "GET";let time = 1.2
         Metrics.setGauge(handler, code, method, time)
        return res.json({status:'можете отправить post'})
    }
    
    async getPasswordFromServer(req, res, next){       //а затем POST, когда отправляет ключ. Если все проверки пройдены - расшифровывается и возращается пароль
        try{
            if (!canGetPas){
                return next(ApiError.forbidden('Нет доступа к post-запросу'))
            }
            let {key}=req.body
            const link1 ='http://localhost:5000/api/decrypt/'+String(req.params.link1)
            const URLs = await Authentication.findOne({
            where: {link:`${link1}`}
            })
            if (!key){
                return next(ApiError.badRequest('Вы не ввели ключ'))
            }
            if(key!=URLs.key){
                return next(ApiError.badRequest('Неверный ключ'))
            }
            //canGetPas=false
            let decrypted_password = decipher(URLs.password)
            let handler = `/api/decrypt/${req.params.link1}`; let code = "200"; let method = "POST";let time = 1.2
            Metrics.setGauge(handler, code, method, time)
            return res.json(decrypted_password)
        }
        catch(e){
            next(ApiError.badRequest(e.message))
        }
        
    }
}
module.exports = new AuthController()