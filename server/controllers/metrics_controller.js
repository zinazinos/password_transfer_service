const client = require('prom-client')
const ApiError=require('../error/apiError')
let gauge = new client.Gauge({                    //Моя метрика server_resp_time
    name: 'server_response_time',
    help: 'Server statistic: handlers, count of requests, responce codes, request exec. time',
    labelNames: [
      'handler',
      'code',
      'method'
    ],
  })

class Metrics{
    
    async setmetrics(req,res,next){     //отправка в prometheus на /metrics
        try{        
            res.set('Content-Type', client.register.contentType)
            res.end(await client.register.metrics())
            //next()
        }
        catch(e){
            //console.log(e.message)
            next(ApiError.badRequest(e.message))
        }       
    }
    setGauge(handler, code, method, time){      //установка значения счетчика gauge извне (из методов контроллеров)
        gauge.set({handler, code, method},  time)
    }
}
module.exports= new Metrics()